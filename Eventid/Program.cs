﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eventid
{

    

    class Program
    {
        static void Main(string[] args)
        {
            #region Delegaadi asi
            //Console.Write("Mida täna teeme k-korrutame või l-liidame");
            //Func<int,int,int> f = null;

            //switch (Console.ReadLine() )
            //{
            //    case "k":
            //        f = (x, y) => x * y;
            //        break;
            //    case "l":
            //        f = (x, y) => x + y;
            //        break;
            //    default:
            //        Console.WriteLine("no siis ei tee midagi");
            //        break;
            //}

            //if ( f != null)
            //{
            //    int x = 0; int y = 1;
            //    while (y > 0 )
            //    {
            //        x = int.Parse(Console.ReadLine());
            //        y = int.Parse(Console.ReadLine());
            //        Console.WriteLine( f(x,y) );
            //    }
            //} 
            #endregion
            Inimene henn = new Inimene() { Nimi = "Henn", Vanus = 62, Linn = "Mõniste" };
            Inimene ants = new Inimene() { Nimi = "Ants", Vanus = 70 };
            henn.Pensionär += Õnnitlus;
            henn.Pensionär += (x,i) => Console.WriteLine($"{x}, sa saad juba {i-65}nda nimelise kella");
            Console.WriteLine(henn);
            henn.Vananeb(); ants.Vananeb();
            
            Console.WriteLine(henn);
            henn.Vananeb(); ants.Vananeb();
            Console.WriteLine(henn);
            henn.Vananeb(); ants.Vananeb();
            Console.WriteLine(henn);
            henn.Vananeb(); ants.Vananeb();
            henn.Pensionär -= Õnnitlus;
            henn.Vananeb(); ants.Vananeb(); henn.Vananeb(); ants.Vananeb(); henn.Vananeb(); ants.Vananeb();


        }



        public static void Õnnitlus(String s, int i)
        {
            Console.WriteLine($"Palju õnne {s}, sa said {i} aadstaseks, võid pinsile minna");
        }


    }
    class Inimene
    {
        private int _Vanus;

        public string Linn; // { get; set; }
        public string Nimi { get; set; }
        public int Vanus { get => _Vanus;
            set { _Vanus = value;
                if (_Vanus > 65)
                { // saadame pensionile
                    if (Pensionär != null) Pensionär(this.Nimi, this.Vanus);
                }
            }
        } 

        public event Action<string,int> Pensionär; 

        public void Vananeb() { this.Vanus++; }

        public override string ToString()
        {
            return $"{Nimi} elab linnas {Linn} ja on {Vanus}-aastane";
        }
    }
}
